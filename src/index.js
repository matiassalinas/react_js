import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

import App from './App';
import AppRelojes from './AppRelojes';
import AppListaTareas from './AppListaTareas';
import AppListaFiltros from './AppListaFiltros';
import AppFacturas from './facturas/AppFacturas';

const baseUrl = process.env.PUBLIC_URL;

ReactDOM.render(
    <BrowserRouter>
        <main>
            <Switch>
                <Route exact path={baseUrl + '/'} component={App} />
                <Route exact path={baseUrl + '/relojes'} component={AppRelojes} />
                <Route exact path={baseUrl + '/lista-tareas'} component={AppListaTareas} />
                <Route exact path={baseUrl + '/lista-filtros'} component={AppListaFiltros} />
                <Route exact path={baseUrl + '/facturas'} component={AppFacturas} />
            </Switch>
        </main>
    </BrowserRouter>
    , document.getElementById('root'));

//registerServiceWorker();
