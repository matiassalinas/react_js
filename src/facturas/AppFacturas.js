import React, { Component } from 'react';
import logo from '../logo.svg';
import '../App.css';
import { Factura, ItemFactura } from './Factura';


class AppFacturas extends Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.crearFactura = this.crearFactura.bind(this);
    this.agregarItemFactura = this.agregarItemFactura.bind(this);
  }

  crearFactura(numero, rc, rv, glosa) {
	  let factura = new Factura(numero, rc, rv, glosa);
	  this.setState({factura: factura});
  }

  agregarItemFactura(glosa, cantidad, valor) {
	  let item = new ItemFactura(glosa, cantidad, valor);
	  this.state.factura.agregarItem(item);
	  this.setState({factura: this.state.factura});
  }

  render() {
    return (
      <div className="App">
        {!this.state.factura && <CrearFactura crearFactura={this.crearFactura} />}
        {this.state.factura && <EncabezadoFactura factura={this.state.factura} />}
        {this.state.factura && <hr />}
        {this.state.factura && <AgregarItemFactura crearItemFactura={this.agregarItemFactura} />}
        {this.state.factura && <hr />}
        {this.state.factura && <ItemsFactura factura={this.state.factura} />}
        {this.state.factura && <hr />}
        {this.state.factura && <TotalesFactura factura={this.state.factura} />}
      </div>
    );
  }
}

class CrearFactura extends Component {

  constructor(props) {
    super(props);
    this.crearFactura = props.crearFactura;
    this.processForm = this.processForm.bind(this);
  }

  processForm(evt) {
    evt.preventDefault(); // Para que el formulario no se mande y no refresque la página
	this.props.crearFactura(this.numero.value, this.rutComprador.value, this.rutVendedor.value, this.glosa.value);
  }


  render() {
    return(
      <div>
        <form onSubmit={this.processForm}>
          <input type="number" ref={n => this.numero = n}         placeholder="Número factura..." id="numeroFactura"/>
          <input type="text"   ref={rc => this.rutComprador = rc} placeholder="Rut Comprador" id="rutComprador"/>
          <input type="text"   ref={rv => this.rutVendedor = rv}  placeholder="Rut Vendedor" id="rutVendedor"/>
          <input type="text"   ref={g => this.glosa = g} placeholder="Glosa Factura" id="glosaFactura"/>
          <button type="submit">Crear Factura</button>
        </form>
      </div>
    )
  }
}

class EncabezadoFactura extends Component {
  render() {
    return(
      <div>
        {this.props.factura.toString()}
        <br/>
        <textarea readOnly value={this.props.factura.glosa} />
      </div>
    )
  }
}

class AgregarItemFactura extends Component {

  constructor(props) {
    super(props);
    this.processForm = this.processForm.bind(this);
  }

  processForm(evt) {
    evt.preventDefault(); // Para que el formulario no se mande y no refresque la página
	this.props.crearItemFactura(this.glosa.value, this.cantidad.value, this.valor.value);
  }

  render() {
    return(
      <div>
        <form onSubmit={this.processForm}>
        <input type="text"   ref={g => this.glosa = g} placeholder="Glosa Item"/>
        <input type="text"   ref={c => this.cantidad = c} placeholder="Cantidad Item"/>
        <input type="text"   ref={v => this.valor = v} placeholder="Valor unitario Item"/>
        <button type="submit">Agregar Item Factura</button>
        </form>
      </div>
    )
  }
}

class ItemsFactura extends Component {
  render() {
    return(
      <div>
        {this.props.factura && this.props.factura.items &&
      <table>
        <tbody>
          <tr>
            <td>Glosa</td>
            <td>Cantidad</td>
            <td>Valor Unitario</td>
            <td>Subtotal</td>
          </tr>
          { this.props.factura.items.map((i, idx) =>
            <tr key={idx}>
              <td>{i.glosa}</td>
              <td>{i.cantidad}</td>
              <td>{i.montoUnitario}</td>
              <td>{i.cantidad * i.montoUnitario}</td>
            </tr>
            )
          }
        </tbody>
      </table>
      }
      </div>
    )
  }
}

class TotalesFactura extends Component {

  constructor(props) {
    super(props);
    this.state = { descuento: 0 };
    this.updateAmounts(props.factura, this.state.descuento);
    this.updateDescuento = this.updateDescuento.bind(this);
  }

  componentWillUpdate(nextProps, nextState) {
    this.updateAmounts(nextProps.factura, nextState.descuento);
  }

  updateAmounts(factura, descuento) {
	  console.log(`UpdateAmounts ${descuento}`);
	  this.valorNeto = factura.valorNeto * (1-descuento/100);
	  this.iva = this.valorNeto * 0.19;
	  this.total = this.valorNeto + this.iva;
  }

  updateDescuento(evt) {
    // Al presionar Enter...
    if(evt.keyCode === 13) {
      console.log(`UpdateDcto: ${this.descuento.value}`);
	  this.setState(prevState => ({
          descuento : this.descuento.value
      }));
    }
  }

  render() {
    return(
      <div>
        {this.props.factura.items.length} items
        <table>
        <tbody>
          <tr>
            <td>% Descuento</td>
            <td>Subtotal Neto</td>
            <td>IVA</td>
            <td>Monto Total</td>
          </tr>
          { this.props.factura &&
            <tr>
              <td>
                <input type="numeric"
                       ref={d => this.descuento = d}
                       onKeyDown={this.updateDescuento}
                       defaultValue={this.state.descuento}
                       />
              </td>
              <td>{this.valorNeto}</td>
              <td>{this.iva}</td>
              <td>{this.total}</td>
            </tr>
          }
        </tbody>
      </table>
      </div>

    )
  }
}

export default AppFacturas;
