export class Factura {

    constructor(numero, rutComprador, rutVendedor, glosa) {
        this.numero = numero;
        this.rutComprador = rutComprador;
        this.rutVendedor = rutVendedor;
        this.glosa = glosa;
        this.items = [];
    }

    agregarItem(item) {
        this.items.push(item);
    }

    get valorNeto() {
        let suma = 0;
        for(let i of this.items) {
            suma += i.montoUnitario * i.cantidad;
        }
        return suma;
    }

    toString() {
        return `Factura #${this.numero} / Comprador: ${this.rutComprador} / Vendedor: ${this.rutVendedor}`;
    }
}

export class ItemFactura {

    constructor(glosa, cantidad, montoUnitario) {
        this.glosa = glosa;
        this.cantidad = cantidad;
        this.montoUnitario = montoUnitario;
    }
}
