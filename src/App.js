import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';

const baseUrl = process.env.PUBLIC_URL;

class App extends Component {
  render() {
    return (
      <div className="App">
        <ul>
          <li><Link to={baseUrl + '/relojes'}>Relojes y Temporizadores</Link></li>
          <li><Link to={baseUrl + '/lista-tareas'}>Lista de Tareas</Link></li>
          <li><Link to={baseUrl + '/lista-filtros'}>Lista con Filtrado</Link></li>
          <li><Link to={baseUrl + '/facturas'}>Gestor de Facturas</Link></li>
        </ul>
      </div>
    );
  }
}

export default App;
