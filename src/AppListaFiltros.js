import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class AppListaFiltros extends Component {

  constructor(props) {
    super(props);
	let items = ["hola", "Chao", "ejemplo", "lista", "queremos un 7"];
	this.state = { items : items, itemsShowing : items }; // items tiene el arreglo original, e itemsShowing los que se van mostrando
	this.keyHandler = this.keyHandler.bind(this);
  }

  render() {
    return (
      <div className="App">
                <div className="App-form">
                  <input type="text" placeholder="Enter text"
                        ref={ text => this.text = text}
                        onKeyUp={ (evt) => this.keyHandler(evt, this.text.value) }
                    />
                </div>
                <ul className="List">
					{this.state.itemsShowing.map( item => (
						<li key={item}>
						{item}</li>))}
            </ul>
            </div>
    );
  }


  keyHandler(evt, text) {
        console.warn(evt.keyCode);
        console.log(text);
	    let newItems = [];
	    // Se recorren los items y se revisa que el texto introducido pertenezca a cada uno de los valores.
	  	for(let item of this.state.items) {
			if (item.includes(text)) {
				newItems.push(item);
			}
		}
	    // Se actualiza el state con los nuevos items a mostrar
	    this.setState(prevState => ({
          itemsShowing: newItems
        }));
  }

}

export default AppListaFiltros;
