import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class AppRelojes extends Component {
  render() {
    return (
      <div className="App">
        <Reloj />
        <Temporizador />
      </div>
    );
  }
}

class Reloj extends Component {

  constructor(props) {
      super(props);
      this.state = { date : new Date() };
  	}

	componentDidMount() { //Cuando el componente se pinta/monta en el browser
        this.idTimer = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.idTimer);
    }

    tick() {
        console.log("tick");
        this.setState({ date : new Date() });
    }

  render() {
    return (
      <div>
        {this.state.date.toLocaleTimeString()}
      </div>
    )
  }
}

class Temporizador extends Component {

  constructor(props) {
        super(props);
        this.state = { seconds : 0 };
    }

    componentDidMount() { //Cuando el componente se pinta/monta en el browser

    }

    componentWillUnmount() {
        clearInterval(this.idTimer);
    }

    tick() {
        console.log("tick_second");
        this.setState({ seconds : this.state.seconds-1 });
        if(this.state.seconds == 0) {
            clearInterval(this.idTimer);
            alert("ding dong");
        }
    }

  render() {
    return(
      <div className="App">
                <div className="App-form">
                   <input type="number" placeholder="Enter seconds" min="1"
                         ref={ seconds => this.seconds = seconds}
                    />
                    <input type="submit" value="OK"
                        onClick={ (evt) => this.clickHandler(evt, this.seconds.value)}
                    />
                </div>
                {this.state.seconds}
            </div>
    );
  }

  clickHandler(evt, seconds) {
        if(seconds <= 0) {
            alert("Segundos inválidos");
            return;
        }
        this.setState({ seconds : seconds });
        this.idTimer = setInterval(
            () => this.tick(),
            1000
        );
    }

}

export default AppRelojes;
