import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class AppListaTareas extends Component {

  constructor(props) {
    super(props);
	this.state = { items : ["1_item", "2_item", "3_item", "4_item"] }; // listado de items
    this.handleSubmit = this.handleSubmit.bind(this);
	this.deleteItem = this.deleteItem.bind(this);
  }
	render() {
        return (
            <div className="App">
                <form onSubmit={ (evt) => this.handleSubmit(evt, this.item.value)}
                 ref="form">
                  <input type="text" placeholder="Nueva tarea"
                    ref={ item => this.item = item}
                  />
                  <button>Agregar tarea</button>
                </form>
				<List items={this.state.items} editItem={ this.editItem } deleteItem={ this.deleteItem } />
            </div>
        );
    }

    handleSubmit(evt, item) {
        evt.preventDefault();
        if (item.length == 0) return;
        this.setState(prevState => ({
          items: prevState.items.concat(item)
        }));
        this.refs.form.reset();
    }

    deleteItem(evt, index) {
        // alert("Eliminar " + index);
		this.setState(prevState => ({
          items: prevState.items.filter( (item, pos) => index !== pos)
        }));
    }

	editItem(evt, index) {
		alert("Editar " + index);
		/*
		*	TO DO
		*/
	}

}

class List extends Component {
    render() {
        return(
            <ul className="List">
                {this.props.items.map( (item, index) => (
                    <li key={index} >
                    <button onClick={ (evt) => this.props.deleteItem(evt, index) }>
					Eliminar
					</button>
              		 <button onClick={ (evt) => this.props.editItem(evt, index) }>
					Editar
					</button>
               		 - {item}</li>
                ))}
            </ul>
        );
    }
}

export default AppListaTareas;
